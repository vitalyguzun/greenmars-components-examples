export default function addStyles(styles) {
    for (const klass in styles) {
        document.querySelectorAll(`.${klass}`).forEach((el) => {
            el.classList.add(styles[klass]);
        });
    }
}
