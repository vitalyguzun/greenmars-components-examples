import { Js360 } from 'js360';
import 'js360/style.css';

import './styles/index.scss';
import * as cardStyles from './styles/card.scss';
import * as js360Styles from './styles/js360.scss';
import addStyles from './utils/AddStyles';

const baseUrl = './assets/js360';

function main() {
    addStyles({ ...cardStyles, ...js360Styles });

    const target = '.js360';

    const js360 = new Js360({
        baseUrl,
        target
    });

    js360.render();
}

document.addEventListener('DOMContentLoaded', main);
